#include"TCP.h"
#include <ws2tcpip.h>
class Tcp
{
public:
	Tcp();
	~Tcp();
	Tcp* NewClient(const char* ip, int port);
	bool Lsiten();
	string ReceiveMsg();
	bool SendMsg();
	bool Connect();
	bool ReceiveConnect();
private:
	SOCKET clientSocket;
	WORD mVersion = MAKEWORD(2, 2);
	WSADATA wsaData;
	char* localIP;
	int localPort;
	char* targetIP;
	int targetPort;
};

Tcp::Tcp()
{

}
Tcp* Tcp::NewClient(const char* ip, int port)
{
	int iResult = 0;
	iResult = WSAStartup(mVersion, &wsaData);

	if (iResult != NO_ERROR)
	{
		printf("WSAStartup failed with error: %d\n", iResult);
		return NULL;
	}

	clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (clientSocket == INVALID_SOCKET)
	{
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
		WSACleanup();
		return NULL;
	}
	else
	{
		wprintf(L"socket function succeeded\n");

		struct in_addr sin_addr;
		inet_pton(AF_INET, ip, (void *)&sin_addr);

		sockaddr_in sSockAddr;
		sSockAddr.sin_addr = sin_addr;
		sSockAddr.sin_port = htons(port);
		sSockAddr.sin_family = AF_INET;

		iResult = bind(clientSocket, (sockaddr*)&sSockAddr, sizeof(sSockAddr));

		if (iResult == SOCKET_ERROR)
		{
			wprintf(L"bind failed with error %u\n", WSAGetLastError());

			iResult = closesocket(clientSocket);
			if (iResult == SOCKET_ERROR)
				wprintf(L"closesocket function failed with error %d\n", WSAGetLastError());
			WSACleanup();
			return NULL;
		}
	}
	return NULL;
}

bool Tcp::Lsiten()
{
	if (listen(clientSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		wprintf(L"listen function failed with error: %d\n", WSAGetLastError());
		return false;
	}

	wprintf(L"Listening on socket...\n");
	return true;
}


bool Tcp::Connect()
{

	return true;
}
bool Tcp::ReceiveConnect()
{
	return true;
}


Tcp::~Tcp()
{
	int iResult = closesocket(clientSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket function failed with error %d\n", WSAGetLastError());
		WSACleanup();
	}
	WSACleanup();
}